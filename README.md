# Jenkins Shared Library

Biblioteca groovy com scripts utilizados nos pipelines gerenciados pelo Jenkins, facilitando a manutenção e gestão dos mesmos.

Tabela de conteúdo
==================

- [Introdução](#introdução)
- [Pré-requisitos](#pré-requisitos)
- [Plugins Requeridos](#plugins-requeridos)
- [Download e Instalação](#download-e-instalação)
- [Parâmetros de Configuração](#parâmetros-de-configuração)
- [Demonstração](#demonstração)
  - [Java Pipeline](#java-pipeline)
- [Referências](#referências)

## Introdução

Esta biblioteca contém todas as etapas necessárias para executar os pipelines Jenkins seguindo as [melhores práticas](https://wiki.jenkins.io/display/JENKINS/Jenkins+Best+Practices), baseando-se nos conceitos de [Jenkins 2.0 Pipelines como Código](https://jenkins.io/solutions/pipeline/).

Mas o grande objetivo desta biblioteca, é o compartilhamento entre os diversos pipelines configurados nos projetos de uma organização, evitando a duplicidade de código-fonte e consequentemente facilitando a manutenção dos pipelines.

Nos próximos tópicos, serão demonstrados o passo-a-passo necessário para configurar e utilizar esta biblioteca em seu ambiente de Integração Contínua.

## Pré-requisitos

Abaixo estão os requisitos necessários para instalar e configurar esta biblioteca:

- [JRE 8](https://www.oracle.com/technetwork/java/javase/jre-8-readme-2095710.html) (Java Runtime Environment)
- Instalação do [Jenkins](https://jenkins.io/) versão 2.132.2 or superior
- Os plugins descritos em [Plugins Requeridos](#plugins-requeridos) 
- Um usuário Jenkins com privilégios de administrador
- Garantir que a instância do Jenkins possui acesso ao [Gitlab](https://gitlab.com/)

## Plugins Requeridos

Os seguintes plugins Jenkins são necessários para usar esta biblioteca. A lista abaixo contém o ID do plugin e sua respectiva versão:

- [Pipeline](https://plugins.jenkins.io/workflow-aggregator) versão 2.6
- [Kubernetes](https://plugins.jenkins.io/kubernetes) versão 1.14.9
- [Gitlab](https://plugins.jenkins.io/gitlab-plugin) versão 1.5.11
- [Slack Notification](https://plugins.jenkins.io/slack) versão 2.20

## Download e Instalação

Para configurar a biblioteca, você precisa executar as seguintes etapas:

1. Autenticar-se na instância do seu Jenkins com privilégio de administrador.

![Página de login do Jenkins](docs/images/JenkinsSharedLibrary-Pagina_de_login_do_Jenkins.png)

2. Após a autenticação, selecione o menu `Manage Jenkins > Configure System`.

![Página com a seleção do menu para configuração do sistema](docs/images/JenkinsSharedLibrary-Pagina_com_a_selecao_do_menu_para_configuracao_do_sistema.png)

3. Em seguida, prossiga até a seção **Global Pipeline Libraries** e habilite o formulário para adição da nova biblioteca clicando no botão `Add button`.

Com o formulário aberto, preencha os campos:
- **Name:** jenkins-library
- **Default version:** master (versão que será utilizada)
- **Load implicitly:** não
- **Allow default version to be overridden:** sim
- **Include @Library changes in job recent changes:** sim
- **Retrieval Method:** Modern SCM
- **Source Code Management:** Git
- **Project Repository:** https://gitlab.com/raphaelfjesus/jenkins-shared-library
- **Credentials:** Selecione ou cadastre a credencial utilizada para acesso ao repositório da Shared Library configurada

![Página para adicionar e configurar a shared library no Jenkins](docs/images/JenkinsSharedLibrary-Pagina_para_adicionar_e_configurar_a_shared_library_no_Jenkins.png)

4. E para concluir, clique em `SAVE` para efetivar as alterações.

Agora a biblioteca está disponível como `jenkins-library` e pode ser usada em qualquer `Jenkinsfile` adicionando esta linha no topo do arquivo:

```groovy
@Library("jenkins-library")_
```

## Parâmetros de Configuração

Abaixo é mostrado todos os parâmetros configurados na Shared Library.

| NOME | VALOR | ESCOPO | DESCRIÇÃO |
|------|-------|--------|-----------|
| projectName | - | público | Nome do projeto. Se não informado, o nome do repositório Git será utilizado. |
| projectGroup | - | público | Grupo no qual o projeto pertence, servindo de namespace para as imagens Docker constrúidas. |
| projectHealthUrl | - | público | URL utilizada para verificar a saúde da aplicação. |

>**Nota:** Todos os parâmetros com escopo **público** pode ter seu valor sobrescrito através do `Jenkinsfile` do projeto.

## Demonstração

Nesta seção será demonstrado a utilização da Shared Library nos projetos de sua organização, de acordo com a linguagem adotada para cada projeto.

>**Nota:** Somente projetos codificados com a [linguagem Java](https://pt.wikipedia.org/wiki/Java_(linguagem_de_programa%C3%A7%C3%A3o)) e usando o [Maven](https://pt.wikipedia.org/wiki/Apache_Maven) como gerenciador de dependências são suportados.

Antes de prosseguirmos com a configuração do pipeline para cada tipo de linguagem, vamos entender a estrutura comum de um pipeline para uma Integração Contínua:

```groovy
// Jenkinsfile
pipeline {
  stages {

    /**
     * Clone the source code of the repository configured for the pipeline.
     */
    stage("Checkout") {
      steps {
        echo "code here..."
      }
    }

    /**
     * Checks if the tools needed to run the pipeline are available in the pipeline
     * execution environment.
     */
    stage("Verify Tools") {
      steps {
        echo "code here..."
      }
    }

    /**
     * Builds the artifact (.jar) of the project, making it available for the
     * next stages of the pipeline.
     */
    stage("Build Artifact") {
      steps {
        echo "code here..."
      }
    }

    /**
     * Run the tests implemented for the project, based on the artifact built in
     * the previous step.
     */
    stage("Test Artifact") {
      parallel {
        stage("Unit Tests") {
          steps {
            echo "code here..."
          }
        }
        stage("Integration Tests") {
          steps {
            echo "code here..."
          }
        }
      }
    }

    /**
     * Scans the application source code for security vulnerabilities, bugs, source code
     * duplicity, and other quality metrics.
     */
    stage("Sonar Analysis") {
      steps {
        echo "code here..."
      }
    }

    /**
     * Builds the application's docker image based on the artifact previously built.
     */
    stage("Build Image") {
      steps {
        echo "code here..."
      }
    }

    /**
     * Run a container from the constructed image and verifies that the application
     * is healthy, from a check URL.
     */
    stage("Test Image") {
      steps {
        echo "code here..."
      }
    }

    /**
     * Run the application performance test, ensuring that the image settings are
     * adhering to the project needs.
     */
    stage("Performance Analysis") {
      steps {
        echo "code here..."
      }
    }

    /**
     * Provides the Docker image of the bundled and tested application to the Docker
     * Registry configured for the project.
     */
    stage ("Push Image") {
      steps {
        echo "code here..."
      }
    }

    /**
     * It provides the docker image for deployment in the staging and production
     * environment, and generates a tag and submits it to the source code repository.
     */
    stage("Release Image & Tag") {
      steps {
        echo "code here..."
      }
    }
  }

  post {
    success {
      echo "code here..."
    }
    failure {
      echo "code here..."
    }
  }
}
```

### Java Pipeline

A configuração do pipeline para um projeto Java, construído sobre a plataforma Docker e orquestrado pelo Kubernetes, poderia ser semelhante a essa:

```groovy
#!/usr/bin/env groovy

/**
 * Before executing this pipeline, it is necessary to create the Persistent Volume Claim 
 * object called "maven-repo" in the Kubernetes cluster, because it will be used to store 
 * the dependencies downloaded by maven, being reused in the next executions of this pipeline 
 * and others that reference it. The following is an example of the contents of a Persistent 
 * Volume Claim:
 * 
 * <code>
 *   apiVersion: "v1"
 *   kind: "PersistentVolumeClaim"
 *   metadata: 
 *     name: "maven-repo"
 *     namespace: "jenkins"
 *   spec: 
 *     accessModes:
 *     - ReadWriteOnce
 *     resources:
 *       requests:
 *         storage: 10Gi
 *     storageClassName: glusterfs
 * </code>
 */
pipeline {
  agent {
    kubernetes {
      label "demo-spring-boot"
      defaultContainer "ci-tools"
      yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: ci-tools
    image: registry.local.com.br/infra/ci-tools:dind-jdk8-node11-alpine
    imagePullPolicy: IfNotPresent
    tty: true
    securityContext:
      privileged: true
    volumeMounts:
    - name: maven-repo
      mountPath: /root/.m2
  imagePullSecrets:
  - name: registry-local
  volumes:
  - name: maven-repo
    persistentVolumeClaim:
      claimName: maven-repo
  hostAliases:
  - ip: 10.75.200.129
    hostnames:
      - sonar.local.com.br
      - nexus.local.com.br
      - registry.local.com.br
"""
    }
  }

  parameters {
    string(
      name: "PROJECT_NAME",
      defaultValue: "demo-spring-boot",
      description: "Project name")
    string(
      name: "PROJECT_GROUP",
      defaultValue: "example",
      description: "Project group")
    string(
      name: "PROJECT_HEALTH_URL",
      defaultValue: "localhost:8090/actuator/health",
      description: "URL to check application health")
    string(
      name: "SLACK_CHANNEL",
      defaultValue: "#build",
      description: "Slack channel used to receive notifications about builds")
    string(
      name: "GATLING_USERS",
      defaultValue: "20",
      description: "Number of concurrent users that will be used in the performance test")
    string(
      name: "GATLING_RAMP_UP",
      defaultValue: "2",
      description: "Number of users that will be added over a period of time")
    string(
      name: "GATLING_DURATION",
      defaultValue: "60",
      description: "Maximum time allowed for performance test duration")
    string(
      name: "GATLING_THROUGHPUT",
      defaultValue: "100",
      description: "Number of requests per second")
    string(
      name: "GATLING_EXPECTED_RESPONSE_TIME",
      defaultValue: "2000000",
      description: "Expected response time to pass the performance test")
    choice(
      name: "GATLING_EXPECTED_SUCCESS_PERCENTAGE",
      choices: ["90", "75", "50", "25"],
      description: "Expected success percentage to pass the performance test")
  }

  environment {
    PROJECT_NAME = "${params.PROJECT_NAME ?: getProjectName()}"
    PROJECT_GROUP = "${params.PROJECT_GROUP ?: 'dipol'}"
    PROJECT_HEALTH_URL = "${params.PROJECT_HEALTH_URL ?: 'localhost:8090/actuator/health'}"

    DOCKER_REGISTRY_URL = "https://registry.local.com.br"
    DOCKER_REGISTRY_DOMAIN = "${DOCKER_REGISTRY_URL}".replace("https://", "")
    DOCKER_REGISTRY_CREDENTIALS = "registry-token"
    DOCKER_REGISTRY_USERNAME = "jenkins-bot"
    DOCKER_REGISTRY_PASSWORD = credentials("${DOCKER_REGISTRY_CREDENTIALS}")

    SONAR_URL = "https://sonar.local.com.br"
    SONAR_CREDENTIALS = "sonar-token"
    SONAR_LOGIN = credentials("${SONAR_CREDENTIALS}")
    SONAR_PROJECT_KEY = "${PROJECT_NAME}"
    SONAR_QUALITY_GATE_API = "${SONAR_URL}/api/qualitygates/project_status?projectKey=${SONAR_PROJECT_KEY}"
    SONAR_COVERAGE_MEASURE_API = "${SONAR_URL}/api/measures/component?component=${SONAR_PROJECT_KEY}&metricKeys=coverage"
    SONAR_EXPECTED_COVERAGE_PERCENTAGE = "75"

    SLACK_CHANNEL = "${params.SLACK_CHANNEL ?: '#build'}"
    SLACK_CREDENTIALS = "slack-token"

    GITLAB_CREDENTIALS = "gitlab-token"
    GITLAB_TOKEN = credentials("${GITLAB_CREDENTIALS}")

    RELEASE_BRANCH_NAME = "master"
    RELEASE_TAG_PREFIX = "v"
    RELEASE_IMAGE_NAME = "${DOCKER_REGISTRY_DOMAIN}/${PROJECT_GROUP}/${PROJECT_NAME}"
    RELEASE_IMAGE_PREFIX = "release-"
    PREVIEW_IMAGE_PREFIX = "preview-"

    COMMIT_URL = "${getCommitUrl()}"
    COMMIT_HASH = "${getShortCommitHash()}"
    COMMIT_AUTHOR_NAME = "${getChangeAuthorName()}"
    COMMIT_AUTHOR_EMAIL = "${getChangeAuthorEmail()}"

    CI_TOOLS_IMAGE = "registry.local.com.br/infra/ci-tools:dind-jdk8-node11-alpine"
  }

  options {
    timeout time: 60, unit: "MINUTES"
    buildDiscarder(
        logRotator(
        artifactDaysToKeepStr: "",
        artifactNumToKeepStr: "",
        daysToKeepStr: "10",
        numToKeepStr: "20"))
    gitLabConnection("gitlab")
    gitlabBuilds(
        builds: [
          "Checkout",
          "Verify Tools",
          "Build Artifact",
          "Unit Tests",
          "Integration Tests",
          "Sonar Analysis",
          "Build Image",
          "Test Image",
          /*"Performance Analysis",*/
          "Push Image",
          "Release Image & Tag"
        ])
  }

  triggers {
    gitlab(
      triggerOnPush: false,
      triggerOnMergeRequest: true,
      triggerOpenMergeRequestOnPush: "never",
      triggerOnNoteRequest: true,
      noteRegex: "Jenkins please retry a build",
      skipWorkInProgressMergeRequest: true,
      ciSkip: false,
      setBuildDescription: true,
      addNoteOnMergeRequest: true,
      addCiMessage: true,
      addVoteOnMergeRequest: true,
      acceptMergeRequestOnSuccess: false,
      branchFilterType: "NameBasedFilter",
      includeBranchesSpec: "master",
      excludeBranchesSpec: "",
      pendingBuildName: "Jenkins",
      cancelPendingBuildsOnUpdate: true)
  }

  stages {

    stage("Checkout") {
      steps {
        gitlabCommitStatus(env.STAGE_NAME) {
          // The 'checkout scm' alway run in the start pipeline
        }
      }
    }

    stage("Verify Tools") {
      steps {
        gitlabCommitStatus(env.STAGE_NAME) {
          sh """
              docker --version
              node --version
              npm --version
              mvn --version
              git --version
              jq --version
              semantic-release --version
          """
        }
      }
    }

    stage("Build Artifact") {
      steps {
        gitlabCommitStatus(env.STAGE_NAME) {
          sh "mvn -B clean package -DskipTests"
        }
      }
      post {
        success {
          archiveArtifacts artifacts: "**/target/*.jar", allowEmptyArchive: true
        }
      }
    }

    stage("Test Artifact") {
      parallel {
        stage("Unit Tests") {
          steps {
            gitlabCommitStatus(env.STAGE_NAME) {
              sh "mvn -B verify -Pjenkins-ci -DskipITs=true"
            }
          }
          post {
            always {
              junit testResults: "**/target/surefire-reports/*.xml", allowEmptyResults: true
            }
          }
        }
        stage("Integration Tests") {
          steps {
            gitlabCommitStatus(env.STAGE_NAME) {
              sh "mvn -B verify -Pjenkins-ci -Dsurefire.skip=true"
            }
          }
          post {
            always {
              junit testResults: "**/target/failsafe-reports/*.xml", allowEmptyResults: true
            }
          }
        }
      }
    }

    stage("Sonar Analysis") {
      steps {
        gitlabCommitStatus(env.STAGE_NAME) {
          sh """
            mvn -B sonar:sonar \
                -DskipTests \
                -Dsonar.host.url=${env.SONAR_URL} \
                -Dsonar.login=${env.SONAR_LOGIN} \
                -Dsonar.projectKey=${env.SONAR_PROJECT_KEY} \
                -Dsonar.jacoco.reportPaths=target/jacoco-integration-test.exec,target/jacoco-unit-test.exec \
                -Dsonar.junit.reportPaths=target/surefire-reports,target/failsafe-reports
          """

          script {
            def qualityGate = getQualityGateStatus()
            echo "Quality Gate ${qualityGate}"

            if (qualityGate.equalsIgnoreCase("ERROR")) {
              error "Pipeline aborted due to quality gate failure"
            }

            def coverage = getCoveragePercentage()
            echo "Coverage ${coverage.toString()}%"

            if (coverage < new Double(env.SONAR_EXPECTED_COVERAGE_PERCENTAGE)) {
              error "Pipeline aborted due to coverage failure"
            }
          }
        }
      }
    }

    stage("Build Image") {
      steps {
        gitlabCommitStatus(env.STAGE_NAME) {
          sh """
            docker login --username=${env.DOCKER_REGISTRY_USERNAME} --password=${env.DOCKER_REGISTRY_PASSWORD} ${env.DOCKER_REGISTRY_URL} \
            && docker build --tag ${env.RELEASE_IMAGE_NAME}:${getImageVersion()} --build-arg JAR_FILE=${getJarFile()} .
          """
        }
      }
    }

    stage("Test Image") {
      steps {
        gitlabCommitStatus(env.STAGE_NAME) {
          script {
            docker.image("${env.RELEASE_IMAGE_NAME}:${getImageVersion()}").inside {
              timeout(time: 1, unit: "MINUTES") {
                waitUntil {
                  try {
                    sleep 5
                    sh "curl ${env.PROJECT_HEALTH_URL}"
                    return true
                  } catch(error) {
                    return false
                  }
                }
              }
            }
          }
        }
      }
    }

    stage("Performance Analysis") {
      environment {
        GATLING_URL = "http://application:8090"
      }
      steps {
        gitlabCommitStatus(env.STAGE_NAME) {
          script {
            docker.image("${env.RELEASE_IMAGE_NAME}:${getImageVersion()}").withRun { c ->
              docker.image(env.CI_TOOLS_IMAGE).inside("--link ${c.id}:application -v $HOME/.m2:/root/.m2") {
                sh """
                  mvn -B gatling:test \
                      -Dbaseurl=${env.GATLING_URL} \
                      -Dsimulation=ExampleSimulation \
                      -Dusers=${params.GATLING_USERS} \
                      -Drampup=${params.GATLING_RAMP_UP} \
                      -Dduration=${params.GATLING_DURATION} \
                      -Dthroughput=${params.GATLING_THROUGHPUT} \
                      -Dresponsetime=${params.GATLING_EXPECTED_RESPONSE_TIME} \
                      -Dsuccesspercentage=${params.GATLING_EXPECTED_SUCCESS_PERCENTAGE}
                """
              }
            }
          }
        }
      }
    }

    stage ("Push Image") {
      steps {
        gitlabCommitStatus(env.STAGE_NAME) {
          sh """
            docker login --username=${env.DOCKER_REGISTRY_USERNAME} --password=${env.DOCKER_REGISTRY_PASSWORD} ${env.DOCKER_REGISTRY_URL} \
            && docker push ${env.RELEASE_IMAGE_NAME}:${getImageVersion()}
          """
        }
      }
    }

    stage("Release Image & Tag") {
      when {
        expression { env.BRANCH_NAME == env.RELEASE_BRANCH_NAME }
      }
      steps {
        gitlabCommitStatus(env.STAGE_NAME) {
          sh """
              export GITLAB_TOKEN="${env.GITLAB_TOKEN}"
              export GIT_AUTHOR_NAME=jenkins-bot
              export GIT_AUTHOR_EMAIL=jenkins-bot@example.org
              export GIT_COMMITTER_NAME=jenkins-bot
              export GIT_COMMITTER_EMAIL=jenkins-bot@example.org
              echo '
              {
                "branch": "${env.RELEASE_BRANCH_NAME}",
                "plugins": [
                  "@semantic-release/commit-analyzer",
                  "@semantic-release/release-notes-generator",
                  ["@semantic-release/changelog", {
                    "changelogFile": "CHANGELOG.md",
                    "changelogTitle": "Changelog"
                  }],
                  ["@semantic-release/exec", {
                    "prepareCmd": "
                       mvn org.codehaus.mojo:versions-maven-plugin:2.7:set -DnewVersion=\${nextRelease.version} -DgenerateBackupPoms=false \
                       && docker login --username=${env.DOCKER_REGISTRY_USERNAME} --password=${env.DOCKER_REGISTRY_PASSWORD} ${env.DOCKER_REGISTRY_URL} \
                       && docker pull ${env.RELEASE_IMAGE_NAME}:${getImageVersion()} \
                       && docker tag ${env.RELEASE_IMAGE_NAME}:${getImageVersion()} ${env.RELEASE_IMAGE_NAME}:\${nextRelease.version} \
                       && docker push ${env.RELEASE_IMAGE_NAME}:\${nextRelease.version}"
                  }],
                  ["@semantic-release/git", {
                    "assets": [
                      "pom.xml",
                      "CHANGELOG.md"
                    ]
                  }],
                  ["@semantic-release/gitlab", {
                    "assets": []
                  }]
                ]
              }
              ' >> .releaserc
              npx semantic-release --no-ci
          """
        }
      }
    }
  }

  post {
    success {
      sendNotification("Successful")
    }

    failure {
      sendNotification("Failure")
    }
  }
}

/**
 * Returns the repository url. For example:
 *
 * https://gitlab.com/raphaelfjesus/demo-spring-boot
 */
def getRepositoryUrl() {
  return sh(returnStdout: true, script: "git config remote.origin.url").trim().replace(".git", "")
}

/**
 * Returns the full hash of the commit. For example:
 *
 * Full Hash: d82b43f315f39384df9fbd5df70ae1139b80fe4c
 */
def getFullCommitHash() {
  return sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%H'").trim()
}

/**
 * Returns the short hash of the commit. For example:
 *
 * Full Hash: d82b43f315f39384df9fbd5df70ae1139b80fe4c
 * Short Hash: d82b43f
 */
def getShortCommitHash() {
  return getFullCommitHash().take(7)
}

/**
 * Returns the commit URL that triggered the pipeline. For example:
 *
 * https://gitlab.com/raphaelfjesus/demo-spring-boot/commit/d82b43f315f39384df9fbd5df70ae1139b80fe4c
 */
def getCommitUrl() {
  return "${getRepositoryUrl()}/commit/${getFullCommitHash()}"
}

/**
 * Returns the name of the commit author who triggered the pipeline. For example:
 *
 * Raphael F. Jesus
 */
def getChangeAuthorName() {
  return sh(returnStdout: true, script: "git show -s --pretty=%an").trim()
}

/**
 * Returns the email of the commit author who triggered the pipeline. For example:
 *
 * raphaelfjesus@gmail.com
 */
def getChangeAuthorEmail() {
  return sh(returnStdout: true, script: "git show -s --pretty=%ae").trim()
}

/**
 * Returns the jar file built for the project. For example:
 *
 * target/demo-spring-boot-1.0.0.jar
 */
def getJarFile() {
  return sh(returnStdout: true, script: "ls target/*.jar | sort -n | head -1").trim()
}

/**
 * Returns the version for the project. For example:
 *
 * For Release -> release-06f5577
 * For Preview -> preview-06f5577
 */
def getImageVersion() {
  return (env.BRANCH_NAME == env.RELEASE_BRANCH_NAME ? env.RELEASE_IMAGE_PREFIX : env.PREVIEW_IMAGE_PREFIX) + env.COMMIT_HASH
}

/**
 * Returns the Quality Gate value assigned to the project after Sonar analysis. For example:
 *
 * OK = for passed
 * ERROR = for failure
 */
def getQualityGateStatus() {
  return sh(returnStdout: true, script: "curl --silent --insecure --user ${env.SONAR_LOGIN}: '${env.SONAR_QUALITY_GATE_API}' | jq -r '.projectStatus.status'").trim()
}

/**
 * Returns the Coverage percentage assigned to the project after Sonar analysis. For example:
 *
 * 100 = all covered tests
 * 77.9 = partially covered tests
 */
def getCoveragePercentage() {
  def coverage = sh(returnStdout: true, script: "curl --silent --insecure --user ${env.SONAR_LOGIN}: '${env.SONAR_COVERAGE_MEASURE_API}' | jq -r '.component.measures | .[0].value'").trim()
  return new Double(coverage)
}

/**
 * Returns the project name. For example:
 *
 * Variable "currentBuild.fullProjectName" equals "demo-spring-boot/master" then
 *
 * return "demo-spring-boot"
 */
def getProjectName() {
return "${currentBuild.fullProjectName}".split("/")[0].trim()
}

def sendNotification(String buildStatus = "Started") {
  def color = buildStatus == "Successful" ? "#00FF00" : (buildStatus == "Failure" ? "#FF0000" : "FFFF00")
  def subject = "${env.JOB_NAME} | Build #${currentBuild.number} - " + buildStatus + " for ${env.BRANCH_NAME}"
  def message = """
*Build* <${env.BUILD_URL}|#${currentBuild.number}> - ${buildStatus} for branch ${env.BRANCH_NAME}
*Commit Hash* <${env.COMMIT_URL}|${env.COMMIT_HASH}>
*Commit Author* <mailto:${env.COMMIT_AUTHOR_EMAIL}|${env.COMMIT_AUTHOR_NAME}>
  """

  slackSend channel: env.SLACK_CHANNEL, color: color, message: message, botUser: true, tokenCredentialId: env.SLACK_CREDENTIALS
}
```

Agora, toda a configuração demonstrada acima poderia ser simplesmente substituída por isso:

```groovy
#!/usr/bin/env groovy
@Library("jenkins-library")_

javaPipeline {
  projectName = "demo-spring-boot"
  projectHealthUrl = "localhost:8090/actuator/health"
}
```

## Referências

- [Jenkins - Shared Library](https://jenkins.io/doc/book/pipeline/shared-libraries/)
- [Jenkins - Boas práticas com pipelines](https://wiki.jenkins.io/display/JENKINS/Jenkins+Best+Practices)
- [Groovy - Configuração do plugim maven para funcionamento no Eclipse](https://github.com/groovy/groovy-eclipse/wiki/Groovy-Eclipse-Maven-plugin)
- [Jenkins Pipeline - Biblioteca de scripts](https://github.com/buildit/jenkins-pipeline-libraries)

